﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth;                            
                                      
    public Slider healthSlider;
    public Image damageImage;
    
    public float flashSpeed;
    public Color flashColour;
    public int currentHealth;

    AudioSource[] audioSources;
    Animator anim;                                              
    PlayerController playerController; 
    bool isDead;                                                
    bool damaged;
    GameController gameController;
    GameObject[] enemies;
    GameObject[] hazards;
    AudioSource damageClip;
    AudioSource deathClip;
   




    void Start()
    {

       
        anim = GetComponent<Animator>();
        audioSources = GetComponents<AudioSource>();
        damageClip = audioSources[2];
        deathClip = audioSources[1];
        playerController = GetComponent<PlayerController>();
        currentHealth = startingHealth;

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }


    void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
           
            
        }
     
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
            
        }

        
        damaged = false;
       
    }


    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;
        healthSlider.value = currentHealth;
        
        damageClip.Play();
        

        if (currentHealth <= 0 && !isDead)
        {
            Death();
            DestroyAllEnemies();
            gameController.GameOver();
        }
    }

    public void DestroyAllEnemies()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        hazards = GameObject.FindGameObjectsWithTag("Hazard");

        for (int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i]);
        }

        for(int i = 0; i < hazards.Length; i++)
        {
            Destroy(hazards[i]);
        }
    }

    void Death()
    {
        isDead = true;
        anim.SetTrigger("Die");

        deathClip.Play();
        playerController.enabled = false;
    }
}
