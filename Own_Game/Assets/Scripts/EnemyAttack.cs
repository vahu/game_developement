﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {
    public int shotDamage;
   
    PlayerHealth playerHealth;
   
    private GameObject[] playerObjects;


    private void Start()
    {
        playerHealth = null;
        playerObjects = GameObject.FindGameObjectsWithTag("Player");
        if (playerObjects.Length != 0)
        {
            
            for (int i = 0; i < playerObjects.Length; i++)
                {
                    if (playerObjects[i].GetComponent<PlayerHealth>() != null)
                {
                    playerHealth = playerObjects[i].GetComponent<PlayerHealth>();
                }
                }
        }
        if (playerObjects.Length == 0)
        {
            Destroy(GameObject.FindWithTag("Enemy"));
            Debug.Log("Player is Dead");
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary" || other.CompareTag("Enemy"))
        {
            return;
        }
        if (other.CompareTag("Player"))
        {
            playerHealth.TakeDamage(shotDamage);
        }
       
    }

  
}
