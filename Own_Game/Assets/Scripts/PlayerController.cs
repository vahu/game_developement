﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float yMin, yMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour {
    private Rigidbody rb;
    private AudioSource audioSource;

    public float speed;
    public Boundary boundary;

    public GameObject shot;
    public Transform fireSpawn;
    public float fireRate;

    private float nextFire;

    private void Update()
    {
        audioSource = GetComponent<AudioSource>();
        if(Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, fireSpawn.position, fireSpawn.rotation);
            audioSource.Play();
        }
        
    }
    void FixedUpdate()
    {
        rb = GetComponent<Rigidbody>();
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(0.0f, moveVertical, moveHorizontal);
        rb.velocity = movement*speed;

        rb.position= new Vector3(0.0f,Mathf.Clamp(rb.position.y,boundary.yMin,boundary.yMax),Mathf.Clamp(rb.position.z,boundary.zMin,boundary.zMax));
        

    }
}
