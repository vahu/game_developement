﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public float levelStartDelay;
    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text highScoreText;

    private int score;
    private GameObject player;
    private PlayerController playerController;
    private bool gameOver;
    private bool restart;
    private bool doingSetUp = true;
    private GameObject levelImage;
    private DataController dataController;
    private Scene currentScene;
    private string sceneName;
    AudioSource[] audioSources;
    AudioSource levelChangeClip;
   


    private void Start()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponentInParent<PlayerController>();
        audioSources = GetComponents<AudioSource>();
        levelChangeClip = audioSources[1];
        dataController = FindObjectOfType<DataController>();
        score = dataController.GetCurrentScore();
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        highScoreText.text = "High Score: " + dataController.GetHighestPlayerScore();

        UpdateScore();
        StartCoroutine(SpawnWaves());
        InitGame();
        
    }

    void LoadLevel()
    {
        
        if (score >= 100 && score < 200)
        {
            if (sceneName.Equals("level 2"))
            {
                return;
            }
            else
            {
                SceneManager.LoadScene("level 2");
            }
        }

        if (score >= 200 && score < 350)
        {
            if (sceneName.Equals("level 3")){
                return;
            }
            else
            {
                
                SceneManager.LoadScene("level 3");
            }
        }

        if (score >= 350 && score < 550)
        {
            if (sceneName.Equals("level 4")){
                return;
            }
            else
            {
                
                SceneManager.LoadScene("level 4");
            }
        }

        if (score >= 550 && score < 750)
        {
            if (sceneName.Equals("level 5")){
                return;
            }
            else
            {
              SceneManager.LoadScene("level 5");
            }
        }

        if (score >= 950 && score < 1250)
        {
            if (sceneName.Equals("level 6")){
                return;
            }
            else
            {
                SceneManager.LoadScene("level 6");
            }
        }
        if (score >= 1250)
        {
            if (sceneName.Equals("level 7")){
                return;
            }
            else
            {
                   SceneManager.LoadScene("level 7");
            }
            
        }
    }
    private void Update()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;

        if (doingSetUp)
        {
            return;
        }
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                score = 0;
                EndGame();
                SceneManager.LoadScene("level 1");
                
            }
        }
        LoadLevel();



    }

    void InitGame()
    {
        doingSetUp = true;
        if (playerController != null) { 
        playerController.enabled = false;
        }
        else
        {
            Debug.Log("No playerController found");
        }
        levelImage = GameObject.Find("LevelImage");
        if(!sceneName.Equals("level 1"))
        {
            levelChangeClip.Play();
        }
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);
        
    }

    void HideLevelImage()
    {
        
        levelImage.SetActive(false);
        doingSetUp = false;
        playerController.enabled = true;
        
    }



    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for(int i = 0;i< hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0,hazards.Length)];
                Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
                //Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, hazard.transform.rotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                EndGame();
                break;
            }
        }
        
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        dataController.SubmitNewPlayerScore(score);
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void EndGame()
    {
        dataController.SubmitNewPlayerScore(score);
    }
}
