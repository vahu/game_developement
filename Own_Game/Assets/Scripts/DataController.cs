﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.Playables;

public class DataController : MonoBehaviour {
    private PlayerProgress playerProgress;

	void Start () {
        DontDestroyOnLoad(gameObject);
        LoadPlayerProgress();
        playerProgress.currentscore = 0;
        SceneManager.LoadScene("level 1");


    }

   

    public void SubmitNewPlayerScore(int newScore)
    {
        playerProgress.currentscore = newScore;
        if (newScore > playerProgress.highestScore)
        {
            playerProgress.highestScore = newScore;
            
        }
        SavePlayerProgress();
    }
	
    public int GetHighestPlayerScore()
    {
        return playerProgress.highestScore;
    }

    public int GetCurrentScore()
    {
        return playerProgress.currentscore;
    }

   

    private void LoadPlayerProgress()
    {
        playerProgress = new PlayerProgress();

        if (PlayerPrefs.HasKey("highestScore"))
        {
            playerProgress.highestScore = PlayerPrefs.GetInt("highestScore");
        }
        if (PlayerPrefs.HasKey("currentScore"))
        {
            playerProgress.currentscore = PlayerPrefs.GetInt("currentScore");
        }
        
    }

    private void SavePlayerProgress()
    {
        PlayerPrefs.SetInt("highestScore", playerProgress.highestScore);
        PlayerPrefs.SetInt("currentScore", playerProgress.currentscore);
      
    }
	
}
